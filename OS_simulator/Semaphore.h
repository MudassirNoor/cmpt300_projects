//
// Created by Mudassir Noor on 2020-03-11.
//

#ifndef SEMAPHORE_H
#define SEMAPHORE_H

#include "List.h"
#include "PCB.h"

typedef struct Semaphore{
    int _id;
    int _value;
    List* _processesBlocked;
} Semaphore;

/**
 * Allocates a new semaphore with the give id value. Instantiates the semaphore with 1
 * @param id id value for semaphore
 * @param value semaphore initial value
 * @return semaphore
 */
Semaphore* CreateSemaphore(int id, int value);

/**
 * Return semaphore ID
 * @param semaphore
 * @return int
 */
int GetSemaphoreID(Semaphore* semaphore);

/**
 * Decrements semaphore value and if value <= 0, process is blocked on that semaphore
 * @param semaphore Semaphore responsible for blocking process
 * @param process process to block
 */
void P(Semaphore* semaphore, PCB* process);

/**
 * Releases processes waiting on semaphore and increments the semaphore value
 * @param semaphore Semaphore blocking the process
 * @return PCB* process
 */
PCB* V(Semaphore* semaphore);

/**
 * Compares semaphore value with the given value
 * @param semaphore
 * @param id
 * @return 1 on match and -1 on failure
 */
int SemaphoreComparator(Semaphore* semaphore, int id);

/**
 * Deallocate memory for semaphore
 * @param semaphore
 * @return
 */
void* FreeSemaphore(Semaphore* semaphore);

#endif //SEMAPHORE_H
