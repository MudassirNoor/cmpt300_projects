//
// Created by Mudassir Noor on 2020-03-11.
//

#ifndef PCB_H
#define PCB_H

#include "List.h"

/*
 * Note:
 * - The message pointer might need to turn into a list of message if a process
 *   can receive multiple processes.
 */

typedef enum State {
    running,
    ready,
    blocked,
    killed //Only when the init is killed
} State;

typedef enum Priority {
    high,
    medium,
    low
} Priority;

typedef struct Message{
    int _senderPid;
    void* _text;
} Message;

typedef struct PCB {
    int _pid;
    int _cpuBursts;
    Priority _priority;
    State _state;
    List* _messages;
} PCB;

/**
 * Creates a new process with state set to ready
 * @param pid int Process ID
 * @param priority enum Priority
 * @return new created process
 */
PCB* CreateProcess(Priority priority);

/**
 * Returns process id of the process
 * @param pcb PCB
 * @return int process Id
 */
int GetProcessID(PCB* pcb);

/**
 * Return priority level of the process
 * @param pcb PCB
 * @return enum Priority
 */
Priority GetPriority(PCB* pcb);

/**
 * Returns current state of the process (i.e ready, running, blocked, deadlocked)
 * @param pcb PCB
 * @return enum State
 */
State GetState(PCB* pcb);

/**
 * Get number of CPU bursts the process has had
 * @param pcb
 * @return int
 */
int GetCPUBursts(PCB* pcb);

/**
 * Returns message stored inside the process.
 * @param pcb PCB
 * @return Message* message struct containing the sendId
 */
Message* GetMessage(PCB* pcb);

/**
 * Set priority level of the current process
 * @param pcb PCB
 * @param priority enum Priority
 */
void SetPriority(PCB* pcb, Priority priority);

/**
 * Sets process id of the process
 * @param pcb PCB
 * @param id unsigned long
 */
void SetProcessID(PCB* pcb, int id);

/**
 * Sets state of the process
 * @param pcb PCB
 * @param state enum State
 */
void SetState(PCB* pcb, State state);

/**
 * Increments CPU bursts
 * @param pcb
 */
void IncrementBursts(PCB* pcb);

/**
 * Stores custom messages
 * @param pcb PCB
 * @param pid sender ID
 * @param message void*
 */
void StoreMessage(PCB* pcb, int pid, void* message);

/**
 * Reports process information from the PCB
 * @param pcb PCB
 */
void ProcessInfo(PCB* pcb);

/**
 * Deallocate memory space for the process
 * @param process Pointer to PCB struct
 * @return 1 on success and -1 on failure
 */
void* FreeProcess(PCB* process);

/**
 * Deallocate memory space for the message
 * @param message pointer to Message struct
 * @return
 */
void* FreeMessage(Message* message);

/**
 * Compares the process ID
 * @param process
 * @param pid
 * @return 1 on success and -1 on failure
 */
int ProcessComparator(PCB* process, int pid);

#endif //PCB_H