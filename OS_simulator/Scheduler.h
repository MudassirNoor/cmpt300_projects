//
// Created by Mudassir Noor on 2020-03-11.
//

#ifndef SCHEDULER_H
#define SCHEDULER_H

#include "List.h"
#include "PCB.h"
#include "Semaphore.h"

typedef struct Scheduler {
    PCB* _executingProcess;
    PCB* _init;
    List* _highPriorityQueue;
    List* _mediumPriorityQueue;
    List* _lowPriorityQueue;
    List* _processesWaitingForReply;
    List* _processesWaitingOnReceive;
    List* _semaphores;
    int _processesReadyOnQueue;
    int _processesBlocked;
} Scheduler;

/**
 * Creates a scheduler struct with empty queues
 * @return Scheduler
 */
Scheduler CreateScheduler();

/**
 * Picks the next process to execute from the priority queues. Preference to higher priority queues
 * @param scheduler Scheduler
 * @return 1 on success or -1 on failure (if no processes are on queue)
 */
int ExecuteNextProcess(Scheduler* scheduler);

/**
 * Adds process to appropriate priority queue. If unable to add, free the memory allocated for the process
 * @param priority enum Priority of the process to determine the queue to add the process to.
 * @param scheduler Scheduler.
 * @return process ID on success, -1 on failure, and return 1 if booting the INIT process
 */
int AddProcessToQueue(PCB* process, Scheduler* scheduler);

/**
 * Forks the currently executing process and adds it to the queue defined by the priority level of the original process.
 * @param scheduler Scheduler
 * @return process ID on success and -1 on failure
 */
int ForkProcess(Scheduler* scheduler);

/**
 * Kills the specified process and removes it from the queue or if it is the running process, calls Exit()
 * @param process PCB process to kill
 * @param scheduler Scheduler.
 * @return 0 on success and -1 on failure
 */
int Kill(int pid, Scheduler* scheduler);

/**
 * Kills the currently executing process and returns process ID of new running process
 * @param scheduler Scheduler
 * @return ID of new running process
 */
int Exit(Scheduler* scheduler);

/**
 * The current executing process is placed on its appropriate queue and the next one is executed.
 * @param scheduler Scheduler
 * @return Process ID of new running process
 */
int Quantum(Scheduler* scheduler);

/**
 * Sends a message on behalf of the executing process to specified process. Blocks the executing process until a reply
 * is received.
 * @param pid Process ID to send the message to
 * @param message Message to send
 * @param scheduler Scheduler
 * @return 1 on success and -1 on failure
 */
int Send(int pid, void* message, Scheduler* scheduler);

/**
 * Receives messages for the executing process. If no message is there to be received, it is blocked until a
 * message is received
 * @param scheduler
 */
void Receive(Scheduler* scheduler);

/**
 * Reply to sender process.
 * @param pid Process ID of process to reply to
 * @param message Message char*
 * @param scheduler Scheduler
 */
int Reply(int pid, void* message, Scheduler* scheduler);

/**
 * Create a new semaphore with the given ID
 * @param semID
 * @param value
 * @param scheduler
 * @return 1 on success and -1 on failure
 */
int AddNewSemaphore(int semID, int value, Scheduler* scheduler);

/**
 * Blocks executing process on the given semaphore ID
 * @param semID ID value of the semaphore
 * @param scheduler
*/
int ExecutePOnGivenSemaphore(int semID, Scheduler* scheduler);

/**
 * Unblocks a process waiting on the semaphore and puts it on the ready queue
 * @param semID ID value of the semaphore
 * @param scheduler
*/
int ExecuteVOnGivenSemaphore(int semID, Scheduler* scheduler);
/**
 * Finds and returns a matching process in the scheduler
 * @param pid process to search for
 * @param scheduler
 * @return process
 */
PCB* FindProcess(int pid, Scheduler* scheduler);

#endif //SCHEDULER_H
