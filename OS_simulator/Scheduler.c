//
// Created by Mudassir Noor 2020-03-11.
//

#include <stdio.h>
#include <string.h>
#include "Scheduler.h"
#include "Helpers.h"

#define INIT 0

void NewExecutionReport(Scheduler* scheduler) {
    SetState(scheduler->_executingProcess, running);
    if (GetProcessID(scheduler->_executingProcess) != INIT) {
        scheduler->_processesReadyOnQueue--;
    }

    BorderLine();
    printf("New executing process:\n");
    ProcessInfo(scheduler->_executingProcess);
}

void BlockAndExecuteNextProcess(Scheduler* scheduler, char* blockType) {
    if (GetProcessID(scheduler->_executingProcess) == INIT) {
        printf("Cannot block INIT process.\n");
        return;
    }

    int status;
    if (strcmp(blockType, "SEND") == 0) {
        status = ListPrepend(scheduler->_processesWaitingForReply, scheduler->_executingProcess);
        if (status == -1) {
            BorderLine();
            printf("No free space available. This process will be killed.\n");
        }
        else {
            BorderLine();
            printf("Process: %d is blocked until a Reply is received.\n", GetProcessID(scheduler->_executingProcess));
        }
    }
    else {
        status = ListPrepend(scheduler->_processesWaitingOnReceive, scheduler->_executingProcess);
        if (status == -1) {
            BorderLine();
            printf("No free space available. This process will be killed.\n");
        }
        else {
            BorderLine();
            printf("Process: %d is blocked until a Message is received.\n", GetProcessID(scheduler->_executingProcess));
        }
    }

    SetState(scheduler->_executingProcess, blocked);
    scheduler->_processesBlocked++;
    ExecuteNextProcess(scheduler);
}

int UnblockProcess(int pid, Scheduler* scheduler, void* message) {
    PCB *process;
    process = ListSearch(scheduler->_processesWaitingForReply, (int (*)(void *, void *)) ProcessComparator, pid);
    if (process != NULL) {
        process = ListRemove(scheduler->_processesWaitingForReply);
        StoreMessage(process, GetProcessID(scheduler->_executingProcess), message);
        AddProcessToQueue(process, scheduler);
        scheduler->_processesBlocked--;
        printf("Process: %d is unblocked.\n", pid);
        return 1;
    }
    process = ListSearch(scheduler->_processesWaitingOnReceive, (int (*)(void *, void *)) ProcessComparator, pid);
    if (process != NULL) {
        process = ListRemove(scheduler->_processesWaitingOnReceive);
        AddProcessToQueue(process, scheduler);
        scheduler->_processesBlocked--;
        printf("Process: %d is unblocked.\n", pid);
        return 1;
    }

    return -1;
}

Scheduler CreateScheduler() {
    Scheduler scheduler;
    PCB* init = CreateProcess(low);
    scheduler._executingProcess = init;
    scheduler._init = init;
    scheduler._highPriorityQueue = ListCreate();
    scheduler._lowPriorityQueue = ListCreate();
    scheduler._mediumPriorityQueue = ListCreate();
    scheduler._processesWaitingOnReceive = ListCreate();
    scheduler._processesWaitingForReply = ListCreate();
    scheduler._semaphores = ListCreate();
    scheduler._processesReadyOnQueue = 0;
    scheduler._processesBlocked = 0;
    SetState(scheduler._executingProcess, running);

    return scheduler;
}

int ExecuteNextProcess(Scheduler* scheduler) {
    if (ListCount(scheduler->_highPriorityQueue) > 0) {
        scheduler->_executingProcess = ListTrim(scheduler->_highPriorityQueue);
    }
    else if (ListCount(scheduler->_mediumPriorityQueue) > 0) {
        scheduler->_executingProcess = ListTrim(scheduler->_mediumPriorityQueue);
    }
    else if (ListCount(scheduler->_lowPriorityQueue) > 0) {
        scheduler->_executingProcess = ListTrim(scheduler->_lowPriorityQueue);
    }
    else {
        scheduler->_executingProcess = scheduler->_init;
    }

    NewExecutionReport(scheduler);
    return 1;
}

int AddProcessToQueue(PCB* process, Scheduler* scheduler) {
    int success;
    Priority priority = GetPriority(process);
    if (priority == high) {
        printf("Process: %d added to high priority queue.\n", GetProcessID(process));
        success = ListPrepend(scheduler->_highPriorityQueue, process);
    }
    else if (priority == medium) {
        printf("Process: %d added to medium priority queue.\n", GetProcessID(process));
        success = ListPrepend(scheduler->_mediumPriorityQueue, process);
    }
    else {
        printf("Process: %d added to low priority queue.\n", GetProcessID(process));
        success = ListPrepend(scheduler->_lowPriorityQueue, process);
    }

    if (success == -1) {
        FreeProcess(process);
        return success;
    }

    scheduler->_processesReadyOnQueue++;
    SetState(process, ready);

    if (GetProcessID(scheduler->_executingProcess) == INIT) {
        SetState(scheduler->_executingProcess, ready);
        ExecuteNextProcess(scheduler);
        return 1; // No need to print out scheduling information for newly added process
    }

    return process->_pid;
}

int ForkProcess(Scheduler* scheduler) {
    if (GetProcessID(scheduler->_executingProcess) == INIT) {
        printf("Cannot FORK the INIT process.\n");
        return -1;
    }
    PCB* forkedProcess = CreateProcess(scheduler->_executingProcess->_priority);
    return AddProcessToQueue(forkedProcess, scheduler);
}

int Kill(int pid, Scheduler* scheduler) {
    if (pid == INIT && GetProcessID(scheduler->_executingProcess) != INIT) {
        printf("You cannot kill the init process when it is not executing.\n");
        return -1;
    }

    if(ProcessComparator(scheduler->_executingProcess, pid) == 1) {
        printf("Process: %d killed successfully.\n", pid);
        return Exit(scheduler);
    }
    if(KillFromQueue(scheduler->_highPriorityQueue, pid) == 1) {
        scheduler->_processesReadyOnQueue--;
        return 1;
    }
    if(KillFromQueue(scheduler->_mediumPriorityQueue, pid) == 1) {
        scheduler->_processesReadyOnQueue--;
        return 1;
    }
    if(KillFromQueue(scheduler->_lowPriorityQueue, pid) == 1) {
        scheduler->_processesReadyOnQueue--;
        return 1;
    }
    if(KillFromQueue(scheduler->_processesWaitingOnReceive, pid) == 1) {
        scheduler->_processesReadyOnQueue--;
        return 1;
    }
    if(KillFromQueue(scheduler->_processesWaitingForReply, pid) == 1) {
        scheduler->_processesReadyOnQueue--;
        return 1;
    }
    if (KillFromSemaphore(pid, scheduler) == 1) {
        scheduler->_processesBlocked--;
        return 1;
    }

    return  -1;
}

int Exit(Scheduler* scheduler) {
    if (GetProcessID(scheduler->_executingProcess) != INIT) {
        FreeProcess(scheduler->_executingProcess);
        ExecuteNextProcess(scheduler);
        return 1;
    }
    if (GetProcessID(scheduler->_executingProcess) == INIT && scheduler->_processesReadyOnQueue > 0) {
        printf("There are other processes in the ready queue. Hence you cannot kill the INIT process.\n");
        return -1;
    }
    else {
        printf("Killing the INIT process.\n");
        SetState(scheduler->_executingProcess, killed);
        scheduler->_executingProcess = NULL;
        return 1;
    }
}

int Quantum(Scheduler* scheduler) {
    if (GetProcessID(scheduler->_executingProcess) == INIT) {
        printf("Cannot expire quantum for the INIT process. Either exit or kill the process.\n");
        return -1;
    }

    // Change priority level to ensure the process does not always get preference if priority is high or medium
    IncrementBursts(scheduler->_executingProcess);
    if (GetCPUBursts(scheduler->_executingProcess) > 2 && GetState(scheduler->_executingProcess) == high) {
        SetPriority(scheduler->_executingProcess, medium);
    }
    else if (GetCPUBursts(scheduler->_executingProcess) > 4 && GetState(scheduler->_executingProcess) == medium) {
        SetPriority(scheduler->_executingProcess, low);
    }
    else if (GetCPUBursts(scheduler->_executingProcess) > 6) {
        SetPriority(scheduler->_executingProcess, high);
    }

    printf("Time quantum for Process : %d has expired!\n", GetProcessID(scheduler->_executingProcess));
    AddProcessToQueue(scheduler->_executingProcess, scheduler);
    return ExecuteNextProcess(scheduler);
}

int Send(int pid, void* message, Scheduler* scheduler) {
    if (pid == INIT) {
        StoreMessage(scheduler->_init, pid, message);
        return 1;
    }

    if (pid == GetProcessID(scheduler->_executingProcess)) {
        printf("Executing process cannot send message to itself.\n");
        return -1;
    }

    PCB* process;

    process = ListSearch(scheduler->_highPriorityQueue, (int (*)(void *, void *)) ProcessComparator, pid);
    if (process != NULL) {
        StoreMessage(process, GetProcessID(scheduler->_executingProcess), message);
        BlockAndExecuteNextProcess(scheduler, "SEND");
        return 1;
    }
    process = ListSearch(scheduler->_mediumPriorityQueue, (int (*)(void *, void *)) ProcessComparator, pid);
    if (process != NULL) {
        StoreMessage(process, GetProcessID(scheduler->_executingProcess), message);
        BlockAndExecuteNextProcess(scheduler, "SEND");
        return 1;
    }
    process = ListSearch(scheduler->_lowPriorityQueue, (int (*)(void *, void *)) ProcessComparator, pid);
    if (process != NULL) {
        StoreMessage(process, GetProcessID(scheduler->_executingProcess), message);
        BlockAndExecuteNextProcess(scheduler, "SEND");
        return 1;
    }
    process = FindInSemaphore(pid, scheduler);
    if (process != NULL) {
        StoreMessage(process, GetProcessID(scheduler->_executingProcess), message);
        BlockAndExecuteNextProcess(scheduler, "SEND");
        return 1;
    }
    process = ListSearch(scheduler->_processesWaitingOnReceive, (int (*)(void *, void *)) ProcessComparator, pid);
    if (process != NULL) {
        StoreMessage(process, GetProcessID(scheduler->_executingProcess), message);
        // Unblock the process blocked on the receiving list first and then block the current process
        UnblockProcess(GetProcessID(process), scheduler, NULL);
        BlockAndExecuteNextProcess(scheduler, "SEND");
        return 1;
    }

    //Processes waiting for replies are not the same as those waiting to receive, hence they are not unblocked
    process = ListSearch(scheduler->_processesWaitingForReply, (int (*)(void *, void *)) ProcessComparator, pid);
    if (process != NULL) {
        StoreMessage(process, GetProcessID(scheduler->_executingProcess), message);
        BlockAndExecuteNextProcess(scheduler, "SEND");
        return 1;
    }

    printf("Process: %d was found.\n", pid);
}

void Receive(Scheduler* scheduler) {
    Message* message = GetMessage(scheduler->_executingProcess);
    if (message == NULL) {
        // INIT process can never block
        if (GetProcessID(scheduler->_executingProcess) != INIT) {
            BlockAndExecuteNextProcess(scheduler, "RECEIVE");
        }
        return;
    }

    BorderLine();
    char fullMessage[60];
    printf("Sender Process: %d\n", message->_senderPid);
    strcpy(fullMessage, "Message Received: ");
    strcat(fullMessage, (const char *) &(message->_text));
    printf("%s", fullMessage);

    FreeMessage(message);
}

int Reply(int pid, void* message, Scheduler* scheduler) {
    if (pid == INIT) {
        StoreMessage(scheduler->_init, pid, message);
        return 1;
    }

    if (pid == GetProcessID(scheduler->_executingProcess)) {
        printf("Executing process cannot send message to itself.\n");
        return 1;
    }

    PCB* process;
    process = ListSearch(scheduler->_processesWaitingForReply, (int (*)(void *, void *)) ProcessComparator, pid);
    if (process != NULL) {
        UnblockProcess(pid, scheduler, message);
        return 1;
    }
    process = ListSearch(scheduler->_highPriorityQueue, (int (*)(void *, void *)) ProcessComparator, pid);
    if (process != NULL) {
        StoreMessage(process, GetProcessID(scheduler->_executingProcess), message);
        return 1;
    }
    process = ListSearch(scheduler->_mediumPriorityQueue, (int (*)(void *, void *)) ProcessComparator, pid);
    if (process != NULL) {
        StoreMessage(process, GetProcessID(scheduler->_executingProcess), message);
        return 1;
    }
    process = ListSearch(scheduler->_lowPriorityQueue, (int (*)(void *, void *)) ProcessComparator, pid);
    if (process != NULL) {
        StoreMessage(process, GetProcessID(scheduler->_executingProcess), message);
        return 1;
    }
    process = FindInSemaphore(pid, scheduler);
    if (process != NULL) {
        StoreMessage(process, GetProcessID(scheduler->_executingProcess), message);
        return 1;
    }
    process = ListSearch(scheduler->_processesWaitingOnReceive, (int (*)(void *, void *)) ProcessComparator, pid);
    if (process != NULL) {
        StoreMessage(process, GetProcessID(scheduler->_executingProcess), message);
        UnblockProcess(GetProcessID(process), scheduler, NULL);
        return 1;
    }

    return -1;
}

int AddNewSemaphore(int semID, int value, Scheduler* scheduler) {
    ListAdd(scheduler->_semaphores, CreateSemaphore(semID, value));
}

int ExecutePOnGivenSemaphore(int semID, Scheduler* scheduler) {
    Semaphore* sem = ListSearch(scheduler->_semaphores, (int (*)(void *, void *)) SemaphoreComparator, semID);
    if (sem == NULL) {
        BorderLine();
        printf("No semaphore with id: %d exists\n", semID);
        return -1;
    }
    P(sem, scheduler->_executingProcess);
    if (scheduler->_executingProcess == NULL) {
        ExecuteNextProcess(scheduler);
    }
    if (GetState(scheduler->_executingProcess) == blocked) {
        BorderLine();
        printf("Process: %d is blocked on Semaphore: %d\n", GetProcessID(scheduler->_executingProcess), semID);
        scheduler->_processesBlocked++;
        ExecuteNextProcess(scheduler);
    }

    return 1;
}

int ExecuteVOnGivenSemaphore(int semID, Scheduler* scheduler) {
    Semaphore* sem = ListSearch(scheduler->_semaphores, (int (*)(void *, void *)) SemaphoreComparator, semID);
    if (sem == NULL) {
        BorderLine();
        printf("No Semaphore: %d exists\n", semID);
        return -1;
    }
    PCB* process;
    process = V(sem);

    if (process == NULL) {
        BorderLine();
        printf("No process was blocked on this semaphore\n");
        return -1;
    }

    scheduler->_processesBlocked--;
    AddProcessToQueue(process, scheduler);
}

PCB* FindProcess(int pid, Scheduler* scheduler) {
    if (pid == INIT) {
        return scheduler->_init;
    }
    if (pid == GetProcessID(scheduler->_executingProcess)) {
        return scheduler->_executingProcess;
    }

    PCB* process;

    process = ListSearch(scheduler->_highPriorityQueue, (int (*)(void *, void *)) ProcessComparator, pid);
    if (process != NULL) {
        return process;
    }
    process = ListSearch(scheduler->_mediumPriorityQueue, (int (*)(void *, void *)) ProcessComparator, pid);
    if (process != NULL) {
        return process;
    }
    process = ListSearch(scheduler->_lowPriorityQueue, (int (*)(void *, void *)) ProcessComparator, pid);
    if (process != NULL) {
        return process;
    }
    process = ListSearch(scheduler->_processesWaitingOnReceive, (int (*)(void *, void *)) ProcessComparator, pid);
    if (process != NULL) {
        return process;
    }
    process = ListSearch(scheduler->_processesWaitingForReply, (int (*)(void *, void *)) ProcessComparator, pid);
    if (process != NULL) {
        return process;
    }
    process = FindInSemaphore(pid, scheduler);
    if (process != NULL) {
        return process;
    }

    return NULL;
}