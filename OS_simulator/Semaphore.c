//
// Created by Mudassir Noor on 2020-03-11.
//

#include "Semaphore.h"
#include <stdlib.h>
#include <stdio.h>
#include "PCB.h"
#include "List.h"

#define INIT 0

Semaphore* CreateSemaphore(int id, int value) {
    Semaphore* sem = malloc(sizeof(Semaphore));
    sem->_id = id;
    sem->_value = value;
    sem->_processesBlocked = ListCreate();
}

int GetSemaphoreID(Semaphore* semaphore) {
    return semaphore->_id;
}

void P(Semaphore* semaphore, PCB* process) {
    if (GetProcessID(process) == INIT) {}
    else if (semaphore->_value <=  0) {
        if (ListPrepend(semaphore->_processesBlocked, process) == 0) {
            SetState(process, blocked);
        }
        else {
            printf("No free space available. The process will be killed.\n");
            FreeProcess(process);
            return;
        }
    }
    printf("Semaphore value decreased.\n");
    semaphore->_value--;
}

PCB* V(Semaphore* semaphore) {
    PCB* process = ListTrim(semaphore->_processesBlocked);
    if (process != NULL) {
        printf("Semaphore value increased.\n");
        semaphore->_value++;
    }
    return process;
}

int SemaphoreComparator(Semaphore* semaphore, int id) {
    if (semaphore->_id == id) {
        return 1;
    }
    return -1;
}

void* FreeSemaphore(Semaphore* semaphore) {
    if (ListCount(semaphore->_processesBlocked) != 0) {
        ListFree(semaphore->_processesBlocked, (void (*)(void *)) FreeProcess);
    }

    free(semaphore);
}
