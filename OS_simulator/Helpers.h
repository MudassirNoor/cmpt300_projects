//
// Created by Mudassir Noor on 2020-03-11.
//

#ifndef HELPERS_H
#define HELPERS_H

#include "List.h"
#include "Scheduler.h"

/**
 * Displays all process queues and their components
 * @param scheduler
 */
void TotalInfo(Scheduler* scheduler);

/**
 * Kills a process from a queue
 * @param queue queue of processes
 * @param pid process to kill
 * @return 1 on success and -1 on failure
 */
int KillFromQueue(List* queue, int pid);
#endif //HELPERS_H

/**
 * Prints a border
 */
void BorderLine();

/**
 * Reads command input from command line
 * @return Valid command
 */
char GetCommandInput();

/**
 * Find a given process in the semaphores within the scheduler
 * @param pid
 * @param scheduler
 * @return Process
 */
PCB* FindInSemaphore(int pid, Scheduler* scheduler);

/**
 * Takes a process off the semaphore and frees the memory
 * @param pid
 * @param scheduler
 * @return 1 on success and -1 on failure
 */
int KillFromSemaphore(int pid, Scheduler* scheduler);

/**
 * Main function that executes the command given along with asking for additional parameters from user
 * @param command
 * @param scheduler
 */
void ExecuteCommand(char command, Scheduler* scheduler);