//
// Created by Mudassir Noor on 2020-03-11.
//

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "Helpers.h"
#include "Scheduler.h"
#include "Semaphore.h"

#define FAIL -1
#define NUMCOMMANDS 13

char commandList[NUMCOMMANDS]  = {'C', 'F', 'K', 'E', 'Q', 'S', 'R', 'Y', 'N', 'P', 'V', 'I' , 'T'};

void ListProcessesOnQueue(List* list) {
    if (list != NULL || ListCount(list) != 0) {
        ListFirst(list);
        while(ListCurr(list) != NULL) {
            printf("Process ID: %d\n", GetProcessID(ListCurr(list)));
            ListNext(list);
        }

        if (ListCount(list) == 0) {
            printf("No processes on this queue.\n");
        }
        ListFirst(list);
    }
}

void TotalInfo(Scheduler* scheduler) {
    BorderLine();
    printf("Executing Process ID: %d\n", GetProcessID(scheduler->_executingProcess));
    printf("INIT process ID: %d\n", GetProcessID(scheduler->_init));
    printf("Total number of processes on Ready Queues: %d\n", scheduler->_processesReadyOnQueue);
    printf("Total number of processes Blocked: %d\n", scheduler->_processesBlocked);

    BorderLine();
    printf("Processes on High Priority Queue:\n");
    ListProcessesOnQueue(scheduler->_highPriorityQueue);

    BorderLine();
    printf("Processes on Medium Priority Queue:\n");
    ListProcessesOnQueue(scheduler->_mediumPriorityQueue);

    BorderLine();
    printf("Processes on Low Priority Queue:\n");
    ListProcessesOnQueue(scheduler->_lowPriorityQueue);

    BorderLine();
    printf("Processes waiting for Replies:\n");
    ListProcessesOnQueue(scheduler->_processesWaitingForReply);

    BorderLine();
    printf("Processes waiting for Receiving messages:\n");
    ListProcessesOnQueue(scheduler->_processesWaitingOnReceive);

    BorderLine();
    printf("Processes blocked on semaphore:\n");
    if (ListCount(scheduler->_semaphores) != 0) {
        ListFirst(scheduler->_semaphores);
        Semaphore* sem;
        while(ListCurr(scheduler->_semaphores) != NULL) {
            sem = ListCurr(scheduler->_semaphores);
            printf("Semaphore ID: %d\n", GetSemaphoreID(sem));
            ListProcessesOnQueue(sem->_processesBlocked);
            ListNext(scheduler->_semaphores);
            printf("\n");
        }
    }
    else {
        printf("No semaphore exists.\n");
    }

}

int KillFromQueue(List* queue, int pid) {
    PCB* process;
    process = ListSearch(queue, (int (*)(void *, void *)) ProcessComparator, pid);
    if (process != NULL){
        process = ListRemove(queue);
        FreeProcess(process);
        printf("Process: %d killed successfully.\n", pid);
        return 1;
    }

    return -1;
}

void BorderLine() {
    printf("====================================================================\n");
}

char GetCommandInput() {
    BorderLine();
    char input = '\0';
    while(1) {
        printf("Enter command: ");
        scanf("%c", &input);
        while ((getchar()) != '\n');
        for(int i = 0; i < NUMCOMMANDS; i++) {
            if (input == commandList[i]) {
                return input;
            }
        }
        printf("Wrong command. Try Again.\n");
        fflush(stdin);
    }
}

int GetIntInput() {
    int input;
    int status = scanf("%d", &input);
    while ((getchar()) != '\n');
    while(status != 1) {
        printf("Please enter a number:\n");
        status = scanf("%d", &input);
        while ((getchar()) != '\n');
    }
    return input;
}

void ExecuteCommand(char command, Scheduler* scheduler) {
    int integerParameters;
    char* textParameters;
    if (command == commandList[0]) {
        printf("Enter process priority (0 = high, 1 = medium, 2 = low): ");
        integerParameters = GetIntInput();
        while (integerParameters != (int)high && integerParameters != (int)medium && integerParameters != (int)low) {
            printf("Not a valid priority. Try Again.\n");
            integerParameters = GetIntInput();
        }
        int status = AddProcessToQueue(CreateProcess((Priority) integerParameters), scheduler);
        if (status == -1) {
            printf("No space available to add another process to queue.\n");
        }
    }
    else if (command == commandList[1]){
        int id = ForkProcess(scheduler);
        if (id == FAIL) {
            printf("Could not FORK the process.\n");
        }
        else {
            BorderLine();
            printf("FORK successful. Forked process ID: %d \n", id);
        }
    }
    else if (command == commandList[2]){
        printf("Enter the process ID of the process you want to KILL: ");
        integerParameters = GetIntInput();
        if (Kill(integerParameters, scheduler) == FAIL) {
            printf("Could not KILL the named process\n");
        }
    }
    else if (command == commandList[3]){
        Exit(scheduler);
    }
    else if (command == commandList[4]){
        Quantum(scheduler);
    }
    else if (command == commandList[5]){
        printf("Enter the process ID to send the message to: ");
        integerParameters = GetIntInput();
        printf("Write message:\n");
        read(0, &textParameters, 40);
        Send(integerParameters, textParameters, scheduler);
    }
    else if (command == commandList[6]){
        Receive(scheduler);
    }
    else if (command == commandList[7]){
        printf("Enter the process ID to reply the message to: ");
        integerParameters = GetIntInput();
        printf("Write message:\n");
        read(0, &textParameters, 40);
        Reply(integerParameters, textParameters, scheduler);
    }
    else if (command == commandList[8]){
        printf("Enter the semaphore ID: ");
        integerParameters = GetIntInput();
        printf("Enter the initial value for semaphore: ");
        int value = GetIntInput();
        AddNewSemaphore(integerParameters, value, scheduler);
    }
    else if (command == commandList[9]) {
        printf("Enter the semaphore ID: ");
        integerParameters = GetIntInput();
        ExecutePOnGivenSemaphore(integerParameters, scheduler);
    }
    else if (command == commandList[10]){
        printf("Enter the semaphore ID: ");
        integerParameters = GetIntInput();
        ExecuteVOnGivenSemaphore(integerParameters, scheduler);

    }
    else if (command == commandList[11]){
        printf("Enter the process ID: ");
        integerParameters = GetIntInput();
        PCB* process = FindProcess(integerParameters, scheduler);
        if (process == NULL) {
            BorderLine();
            printf("No process was found with the ID: %d\n", integerParameters);
        }
        else {
            ProcessInfo(process);
        }
    }
    else {
        TotalInfo(scheduler);
    }
}

PCB* FindInSemaphore(int pid, Scheduler* scheduler) {
    PCB* process;
    ListFirst(scheduler->_semaphores);
    Semaphore* sem;
    while(ListCount(scheduler->_semaphores) > 0 && ListCurr(scheduler->_semaphores) != NULL) {
        sem = ListCurr(scheduler->_semaphores);
        process = ListSearch(sem->_processesBlocked, (int (*)(void *, void *)) SemaphoreComparator, pid);
        if (process != NULL) {
            return process;
        }
        ListNext(scheduler->_semaphores);
    }

    return NULL;
}

int KillFromSemaphore(int pid, Scheduler* scheduler) {
    ListFirst(scheduler->_semaphores);
    Semaphore* sem;
    while(ListCurr(scheduler->_semaphores) != NULL) {
        sem = ListCurr(scheduler->_semaphores);
        if (KillFromQueue(sem->_processesBlocked, pid) == 1) {
            return 1;
        }
        ListNext(scheduler->_semaphores);
    }
}