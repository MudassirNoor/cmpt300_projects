#include <stdio.h>
#include <stdlib.h>
#include "PCB.h"
#include "Scheduler.h"
#include "Semaphore.h"
#include "Helpers.h"

int main() {
    Scheduler scheduler = CreateScheduler();
    int exitSimulation = 0;
    char command;
    BorderLine();
    printf("OS Simulator\n");
    BorderLine();
    printf("Commands:\n");
    printf("C-> creates new process and adds to the queue\n");
    printf("F-> forks the executing process\n");
    printf("K-> kills the named process\n");
    printf("E-> kills the currently executing process\n");
    printf("Q-> expires the time quantum of the current process and executes a new one\n");
    printf("S-> sends a message from the executing process to the named process. Executing process is blocked until a reply is received.\n");
    printf("R-> executing process receives a message. If no message to be received, it is blocked\n");
    printf("Y-> replies to sender process on behalf of the executing process\n");
    printf("N-> creates a semaphore with the given ID and initial value\n");
    printf("P-> Executes the P operation on the given semaphore\n");
    printf("V-> Executes the V operation on the given semaphore\n");
    printf("I-> Displays information on the named process\n");
    printf("T-> Displays all process scheduling information\n");
    BorderLine();
    while (exitSimulation == 0) {
            BorderLine();
            printf("Executing Process ID: %d\n", GetProcessID(scheduler._executingProcess));
            BorderLine();
            command = GetCommandInput();
            ExecuteCommand(command, &scheduler);
            if (GetState(scheduler._init) == killed) {
                FreeProcess(scheduler._init);
                exitSimulation = 1;
            }
    }

    ListFree(scheduler._processesWaitingForReply, (void (*)(void *)) FreeProcess);
    ListFree(scheduler._processesWaitingOnReceive, (void (*)(void *)) FreeProcess);
    ListFree(scheduler._lowPriorityQueue, (void (*)(void *)) FreeProcess);
    ListFree(scheduler._highPriorityQueue, (void (*)(void *)) FreeProcess);
    ListFree(scheduler._mediumPriorityQueue, (void (*)(void *)) FreeProcess);
    ListFree(scheduler._semaphores, (void (*)(void *)) FreeSemaphore);
    free(scheduler._init);

    return 0;
}