//
// Created by Mudassir Noor on 2020-03-11.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "PCB.h"
#include "Helpers.h"

int PID = 0;

void ConvertStateToString(State state) {
    if (state == running) {
        printf("Running\n");
    }
    else if (state == ready) {
        printf("Ready\n");
    }
    else if (state == blocked) {
        printf("Blocked\n");
    }
    else {
        printf("Killed\n");
    }
}

void ConvertPriorityToString(Priority priority) {
    if (priority == high) {
        printf("High\n");
    }
    else if (priority == medium) {
        printf("Medium\n");
    }
    else{
        printf("Low\n");
    }
}

PCB* CreateProcess(Priority priority) {
    PCB* process = malloc(sizeof(PCB));
    process->_cpuBursts = 0;
    SetProcessID(process, PID);
    SetPriority(process, priority);
    SetState(process, ready);
    process->_messages = ListCreate();
    PID++;
    return process;
}

int GetProcessID(PCB* pcb) {
    return pcb->_pid;
}

Priority GetPriority(PCB* pcb) {
    return pcb->_priority;
}

State GetState(PCB* pcb) {
    return pcb->_state;
}

int GetCPUBursts(PCB* pcb) {
    return pcb->_cpuBursts;
}

Message* GetMessage(PCB* pcb) {
    Message* m = ListTrim(pcb->_messages);
    return m;
}

void SetPriority(PCB* pcb, Priority priority) {
    pcb->_priority = priority;
}

void SetProcessID(PCB* pcb, int id) {
    pcb->_pid = id;
}

void SetState(PCB* pcb, State state) {
    pcb->_state = state;
}

void IncrementBursts(PCB* pcb) {
    pcb->_cpuBursts++;
}

void StoreMessage(PCB* pcb, int pid, void* message) {
    Message* newStruct = malloc(sizeof(Message));
    newStruct->_text = message;
    newStruct->_senderPid = pid;
    ListPrepend(pcb->_messages, newStruct);
}

void ProcessInfo(PCB* pcb) {
    BorderLine();
    printf("PROCESS INFORMATION:\n");
    BorderLine();
    printf("Process ID: %d\n", GetProcessID(pcb));
    printf("Process State: ");
    ConvertStateToString(GetState(pcb));
    printf("Process Priority: ");
    ConvertPriorityToString(GetPriority(pcb));
}

void* FreeProcess(PCB* pcb) {
    if (pcb != NULL) {
        if (ListCount(pcb->_messages) != 0) {
            ListFree(pcb->_messages, (void (*)(void *)) FreeMessage);
        }
        free(pcb);
        return (void*) 1;
    }

    return (void*) -1;
}

void* FreeMessage(Message* message) {
    if (message != NULL) {
        free(message);
    }
}

int ProcessComparator(PCB* process, int pid) {
    if (GetProcessID(process) == pid) {
        return 1;
    }

    return -1;
}