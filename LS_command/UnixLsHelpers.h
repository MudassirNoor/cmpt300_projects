//
// Created by Mudassir Noor on 2020-04-09.
//

#ifndef UNIXLSHELPERS_H
#define UNIXLSHELPERS_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pwd.h>
#include <grp.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <errno.h>
#include <memory.h>

#define FAIL -1
#define SUCCESS 0

typedef enum option {
    longListing,
    inode,
    both,
    def,
} option;

typedef struct arguments {
    int pathCount;
    option option;
    char** paths;
} arguments;

arguments* validateArguments(int argc, char* argv[]);
int executeLs(char* path, option option, int printPath);
void determineSelectedOption(option option, struct stat* statBuffer, char* path, char* fileName);
void printInodeNumber(__ino_t st_inode);
void printTypeAndPermissions(__mode_t st_mode);
void printHardLinks(__nlink_t st_nlink);
void printOwnerName(__uid_t st_uid);
void printGroupName(__gid_t st_gid);
void printSizeOfFile(__off_t st_size);
void printTimeStampOfLatestModifications(struct timespec st_mtim);
void printFileName(char* fileName);
void printSymbolicLinkedFileName(char* filePath, char* fileName);

#endif //UNIXLSHELPERS_H