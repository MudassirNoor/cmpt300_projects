//
// Created by Mudassir Noor on 2020-04-09.
//

#include "UnixLsHelpers.h"

int main(int argc, char* argv[]) {
    arguments* args = validateArguments(argc, argv);
    struct stat stat;

    if (!args) {
        fprintf(stderr, "Error in command.\n");
        return 0;
    }

    option option;
    option = args->option;
    int printPath = args->pathCount > 1 ? 1 : 0;
    for (int i = 0; i < args->pathCount; i++) {
        executeLs(args->paths[i], option, printPath);
        printf("\n");
    }

    free(args);
    return 0;
}