//
// Created by Mudassir Noor on 2020-04-09.
//

#include "UnixLsHelpers.h"

arguments* validateArguments(int argc, char* argv[]) {
    arguments* args = NULL;
    if (argc == 1) {
        args = malloc(sizeof(arguments) + sizeof(char*));
        args->option = def;
        args->pathCount = 1;
        args->paths = malloc(1);
        args->paths[0] = "./";
        return args;
    }

    int pathArgumentStarted = 0;
    int hasInodeFlag = 0;
    int hasLongListFlag = 0;
    int path = 0;
    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-i") != 0 && strcmp(argv[i], "-l") != 0 && strcmp(argv[i], "-li") != 0 && strcmp(argv[i], "-il") != 0){
            pathArgumentStarted = 1;
            if (args == NULL) {
                args = malloc(sizeof(arguments) + (argc - i)*sizeof(char*));
                args->pathCount = argc - i;
                args->paths = malloc((size_t) (argc - i));
            }

            args->paths[path] = argv[i];
            path++;
        }
        else {
            if (pathArgumentStarted == 1) {
                free(args);
                return NULL;
            }

            if (strcmp(argv[i], "-i") == 0) hasInodeFlag = 1;
            else if (strcmp(argv[i], "-l") == 0) hasLongListFlag = 1;
            else hasInodeFlag = 1, hasLongListFlag = 1;
        }
    }

    if (args == NULL) {
        args = malloc(sizeof(arguments) + sizeof(char*));
        args->pathCount = 1;
        args->paths = malloc(1);
        args->paths[0] = "./";
    }

    if (hasInodeFlag == 1 && hasLongListFlag == 1) args->option = both;
    else if (hasInodeFlag == 1) args->option = inode;
    else if (hasLongListFlag == 1) args->option = longListing;
    else args->option = def;

    return args;
}

int executeLs(char* path, option option, int printPath) {
    struct stat statBuffer;
    int status = lstat(path, &statBuffer);

    if (status != SUCCESS) {
        printf("UnixLs: cannot access \'%s\': ", path);
        printf("%s\n", strerror(errno));
        return FAIL;
    }

    if (statBuffer.st_mode & S_IFDIR) {
        struct dirent* directoryEntry;
        DIR* directory = opendir(path);
        char fullPath[300];

        if (printPath == 1) printf("%s:\n", path);

        while ((directoryEntry = readdir(directory)) != NULL) {
            if (directoryEntry->d_name[0] == '.') continue;

            strcpy(fullPath, path);
            strcat(fullPath, "/");
            strcat(fullPath, directoryEntry->d_name);
            if(lstat(fullPath, &statBuffer) == FAIL) {
                printf("%s\n", strerror(errno));
                continue;
            }

            switch (statBuffer.st_mode & S_IFMT) {
                case S_IFLNK:
                    determineSelectedOption(option, &statBuffer, fullPath, directoryEntry->d_name);
                    break;
                default:
                    determineSelectedOption(option, &statBuffer, NULL, directoryEntry->d_name);
            }

            fullPath[0] = '\0';
        }

        closedir(directory);
        printf("\n");
    }

    else {
        switch (statBuffer.st_mode & S_IFMT) {
            case S_IFLNK:
                determineSelectedOption(option, &statBuffer, path, path);
                break;
            default:
                determineSelectedOption(option, &statBuffer, NULL, path);
        }
    }
}

void determineSelectedOption(option option, struct stat* statBuffer, char* path, char* fileName) {
    switch (option) {
        case both:
            printInodeNumber(statBuffer->st_ino);
            printTypeAndPermissions(statBuffer->st_mode);
            printHardLinks(statBuffer->st_nlink);
            printOwnerName(statBuffer->st_uid);
            printGroupName(statBuffer->st_gid);
            printSizeOfFile(statBuffer->st_size);
            printTimeStampOfLatestModifications(statBuffer->st_mtim);
            if (!path) printFileName(fileName); //path is only given in case of a symbolic link
            else printSymbolicLinkedFileName(path, fileName);
            break;
        case longListing:
            printTypeAndPermissions(statBuffer->st_mode);
            printHardLinks(statBuffer->st_nlink);
            printOwnerName(statBuffer->st_uid);
            printGroupName(statBuffer->st_gid);
            printSizeOfFile(statBuffer->st_size);
            printTimeStampOfLatestModifications(statBuffer->st_mtim);
            if (!path) printFileName(fileName); //path is only given in case of a symbolic link
            else printSymbolicLinkedFileName(path, fileName);
            break;
        case inode:
            printInodeNumber(statBuffer->st_ino);
            if (!path) printFileName(fileName); //path is only given in case of a symbolic link
            else printSymbolicLinkedFileName(path, fileName);
            break;
        default:
            printf("%s  ", fileName);
    }
}

void printInodeNumber(__ino_t st_inode) {
    printf("%20lu ", st_inode);
}

void printTypeAndPermissions(__mode_t st_mode) {
    switch (st_mode & S_IFMT) {
        case S_IFDIR:
            printf("d");
            break;
        case S_IFLNK:
            printf("l");
            break;
        default:
            printf("-");
    }

    printf((st_mode & S_IRUSR) ? "r" : "-");
    printf((st_mode & S_IWUSR) ? "w" : "-");
    printf((st_mode & S_IXUSR) ? "x" : "-");

    printf((st_mode & S_IRGRP) ? "r" : "-");
    printf((st_mode & S_IWGRP) ? "w" : "-");
    printf((st_mode & S_IXGRP) ? "x" : "-");

    printf((st_mode & S_IROTH) ? "r" : "-");
    printf((st_mode & S_IWOTH) ? "w" : "-");
    printf((st_mode & S_IXOTH) ? "x " : "- ");
}

void printHardLinks(__nlink_t st_nlink) {
    printf("%2lu ", st_nlink);
}

void printOwnerName(__uid_t st_uid) {
    struct passwd *pw;
    pw = getpwuid(st_uid);

    if (pw) {
        printf("%8s ", pw->pw_name);
    }
    else {
        printf("%s\n", strerror(errno));
    }

}

void printGroupName(__gid_t st_gid) {
    struct group *group;
    group = getgrgid(st_gid);

    if (group) {
        printf("%8s ", group->gr_name);
    }
    else {
        printf("%s\n", strerror(errno));
    }
}

void printSizeOfFile(__off_t st_size) {
    printf("%8li ", st_size);
}

void printTimeStampOfLatestModifications(struct timespec st_mtim) {
    struct tm *time = localtime((const time_t *) &st_mtim);
    char timeStamp[256];
    if (time) {
        strftime(timeStamp, sizeof(timeStamp), "%b %d %Y %R", time);
        printf("%s ", timeStamp);
    }
    else {
        printf("%s\n", strerror(errno));
    }
}

void printFileName(char* fileName) {
    printf("%s\n", fileName);
}

void printSymbolicLinkedFileName(char* filePath, char* fileName) {
    struct stat linkStatBuffer;
    char* linkFileName;
    size_t nameSize;

    lstat(filePath, &linkStatBuffer);

    nameSize = (size_t) (linkStatBuffer.st_size + 1);
    linkFileName = malloc(nameSize);

    readlink(filePath, linkFileName, nameSize);
    linkFileName[linkStatBuffer.st_size] = '\0';

    printf("%s -> %s\n", fileName, linkFileName);
    free(linkFileName);
}